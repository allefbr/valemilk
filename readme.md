#FEBRACIS
Repositório do novo portal da FEBRACIS.

##Tecnologias:

- Wordpress
- jQuery
- Gulp
- Sass

##Grid - Jeet

7.0.0 traz grandes novidades. Certifique-se de ler o [guia de migração](https://github.com/mojotech/jeet/wiki/Migrating-from-6-to-7)!

[Jeet](http://jeet.gs) é um grid system simples e fracional para **Sass** e **Stylus**.

####Como usar:

```fish
npm install -D jeet
```

```scss
@import 'node_modules/jeet/scss/index';

.container {
  @include center();
}

.container div {
  @include column(1/3);
}
```
#### Browser Support
- IE9+ sem alterações. IE8+ com Selectivizr. Sempre usar Autoprefixer.

##Plugins Inseridos:

- MetaBox
- Contact Forms + Contact Forms DB
- Pagenav

##API's:
- Mailchimp