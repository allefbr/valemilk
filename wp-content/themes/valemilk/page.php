<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<?php //Custom Fields ?>
<section class="pages">
    <div class="container">
        <div class="pages__header">
            <h2 class="pages__headline">
                <?php the_title();?>
            </h2>
        </div>
        <div class="pages__content">
            <?php the_content();?>
        </div>
    </div>
</section>
<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>