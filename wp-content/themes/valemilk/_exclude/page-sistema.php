<?php /* Template Name: Sistema de Coaching */ ?>
<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<?php //Custom Fields ?>
<section class="pages pages--sistema">
    <div class="container">
        <div class="pages__header">
            <h2 class="pages__headline">
                <?php the_title();?>
            </h2>
        </div>
        <div class="pages__content">
            <?php the_content();?>
        </div>
        <div class="pages__thumb">
            <?php the_post_thumbnail('full');?>
        </div>
    </div>
</section>
<section class="sistema">
    <div class="container">
        <div class="sistema__header">
            <h2 class="sistema__headline">
                <?php the_title('Benefícios do ');?>
            </h2>
        </div>
        <div class="sistema__lista">
            <?php $sistema = get_post_meta(get_the_id(), 'sistema', 'true');
                // print_r($sistema);
                foreach ($sistema as $item) {
                    echo '
                    <div class="sistema__item">
                        <div class="sistema__thumb">';
                            imglazy($item['icone'][0], 'full', 'fade', $item['titulo']);
                        echo '</div>
                        <div class="sistema__titulos">'.$item['titulo'].'</div>
                        <div class="sistema__desc">'.$item['descricao'].'</div>
                    </div>';
                }
            ?>
        </div>
    </div>
</section>
<section class="planos">
    <div class="container">
        <div class="planos__header">
            <h2 class="planos__headline">
                Escolha o plano ideal para você
            </h2>
        </div>
        <div class="planos__lista">
            <?php
                $planos = array('plano_a', 'plano_b', 'plano_c');
                foreach ($planos as $plano) {
                    $titulo  = get_post_meta(get_the_id(), $plano.'_titulo', 'true');
                    $total   = get_post_meta(get_the_id(), $plano.'_total', 'true');
                    $adesao  = get_post_meta(get_the_id(), $plano.'_adesao', 'true');
                    $mensal  = get_post_meta(get_the_id(), $plano.'_mensal', 'true');
                    $acesso  = get_post_meta(get_the_id(), $plano.'_acesso', 'true');
                    $code    = get_post_meta(get_the_id(), $plano.'_code', 'true');
                    $cor     = get_post_meta(get_the_id(), $plano.'_cor', 'true');

                    $item = '<div class="planos__item" style="border-color: '.$cor.'">';
                        $item .= '<div class="planos__titulos" style="background-color: '.$cor.';">'.$titulo.'</div>';
                        $item .= '<div class="planos__total">R$ '.$total.'</div>';
                        $item .= '<div class="planos__adesao">';
                            $item .= 'Adesão: '.($adesao ? 'R$ '.$adesao : 'Grátis');
                        $item .= '</div>';
                        $item .= '<div class="planos__mensal">Mensal: R$ '.$mensal.'</div>';
                        $item .= '<div class="planos__acesso">Acesso por '.$acesso.'</div>';
                        $item .= '<div class="planos__pagueseguro">'.$code.'</div>';
                    $item .= '</div>';
                    echo $item;
                }
            ?>
        </div>
    </div>
</section>
<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>