<?php get_template_part('templates/html','header');?>

<section class="cursos cursos--home">
    <div class="container">
        <div class="cursos__head">
            <h3 class="cursos__headline">Próximos Cursos</h3>

            <?php //PEGA LISTA DE CATEGORIAS DE RECEITAS
            $args_curso = array('taxonomy' => 'formacao', 'orderby' => 'title', 'order' => 'ASC', 'parent' => '0', 'hide_empty' => false);
            $cat_curso = get_categories($args_curso);

            $args_local = array('taxonomy' => 'eventos', 'orderby' => 'title', 'order' => 'ASC', 'parent' => '0', 'hide_empty' => false);
            $cat_local = get_categories($args_local);

            if (!empty($cat_curso) && !is_wp_error($cat_curso) || !empty($cat_local) && !is_wp_error($cat_local)) : ?>
            <div class="cursos__form">
                <form action="">
                    <div class="cursos__select">
                        <select class="cs-select cs-skin-elastic">
                        <option value="" disabled selected>Tipo de Curso</option>
                            <?php //options
                                foreach ( $cat_curso as $categoria ) {
                                    echo '
                                    <option value="'.$categoria->term_id.'" data-class="flag-'.$categoria->slug.'">'.
                                    $categoria->name
                                    .'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="cursos__select cursos__select--last">
                        <select class="cs-select cs-skin-elastic">
                        <option value="" disabled selected>Localização</option>
                            <?php //options
                                foreach ( $cat_local as $categoria ) {
                                    echo '
                                    <option value="'.$categoria->term_id.'" data-class="flag-'.$categoria->slug.'">'.
                                    $categoria->name
                                    .'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <input class="btn cursos__submit" type="submit"  value="Buscar">
                </form>
            </div>
            <?php endif;?>
        </div>
        <div>
        <?php
            $get_data   = date('Y-m-d');
            $get_mes    = date('m');
            $get_ano    = date("Y");
            $args = array(
                'posts_per_page'=> 6,
                'post_type'  => 'agenda',
                'meta_key'   => 'agenda_data',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'relation' => 'OR',
                        array(
                            'key'     => 'agenda_data_fim',
                            'value'   => $get_data,
                            'compare' => '>=',
                        ),
                        array(
                            'key'     => 'agenda_data',
                            'value'   => $get_data,
                            'compare' => '>=',
                        )
                    ),
                    array(
                        'key'     => 'agenda_tipo',
                        'value'   => '2',
                        'compare' => '==',
                    ),
                ), 'orderby' => 'meta_value', 'order' => 'ASC'
            );
            $agenda = new WP_Query($args);
            function formata_data($start, $final){

                    $mes_abrev = array(
                        "1"   => "Jan",
                        "2"   => "Fev",
                        "3"   => "Mar",
                        "4"   => "Abr",
                        "5"   => "Mai",
                        "6"   => "Jun",
                        "7"   => "Jul",
                        "8"   => "Ago",
                        "9"   => "Set",
                        "10"   => "Out",
                        "11"   => "Nov",
                        "12"   => "Dez"
                    );

                    $dateStart = date_parse_from_format('Y-m-d', $start);
                    $dateFinal = date_parse_from_format('Y-m-d', $final);
                    $mes_start = $mes_abrev[$dateStart['month']];
                    $mes_final = $mes_abrev[$dateFinal['month']];

                    if($start == $final) {
                        echo $dateFinal['day'].' de '.
                             $mes_start.' '.
                             $dateFinal['year'];
                    }

                    else if($dateStart['month'] == $dateFinal['month']) {
                        echo $dateStart['day'].' a '.
                             $dateFinal['day'].' '.
                             $mes_start.' '.
                             $dateFinal['year'];
                    }

                    else {
                        if($dateStart['year'] != $dateFinal['year']) :
                            echo $dateStart['day']. ' de '.
                                 $mes_start.' de '.
                                 $dateStart['year'].' à '.
                                 $dateFinal['day'].' de '.
                                 $mes_final.' '.
                                 $dateFinal['year'];
                        else :
                             echo $dateStart['day']. ' de '.
                                 $mes_start.' à '.
                                 $dateFinal['day'].' de '.
                                 $mes_final.' '.
                                 $dateFinal['year'];
                        endif;
                    }
            }

            while ($agenda->have_posts()) : $agenda->the_post();
                $data  = get_post_meta( get_the_id(), 'agenda_data', true);
                $dataf = get_post_meta( get_the_id(), 'agenda_data_fim', true);
                $local = get_post_meta( get_the_id(), 'agenda_local', true);
                $curso = get_post_meta( get_the_id(), 'agenda_curso', true);
                $color = get_post_meta( $curso, 'curso_cor', true);
            ?>

            <div class="cursos__item">
                <div class="cursos__infos">
                    <div class="cursos__local">
                        <i style="color: <?php echo $color;?>;" class="fa fa-map-marker"></i><?php echo $local;?>
                    </div>
                    <div class="cursos__data">
                        <i style="color: <?php echo $color;?>;" class="fa fa-calendar-o"></i><?php formata_data($data, $dataf);?>
                    </div>
                    <div class="cursos__titulo"><?php echo get_the_title($curso);?></div>
                    <div class="cursos__btn">
                        <a href="<?php the_permalink();?>" class="btn" style="border-color: <?php echo $color;?>;">Saiba mais</a>
                    </div>
                </div>
                <div class="cursos__thumb">
                    <?php //echo $curso; ?>
                    <?php thumblazy($curso, 'full', 'fade', get_the_title($curso));?>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata();?>
        </div>
    </div>
</section>

<?php get_template_part('templates/html','footer');?>