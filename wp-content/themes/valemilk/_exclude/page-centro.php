<?php /* Template Name: Centro Conceito */ ?>
<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<?php //CUSTOM FIELDS
    $museu     = get_post_meta( get_the_id(), '', 'true');
?>
<section class="centro centro--page">
    <div class="centro__wrap">
        <div class="container">
            <div class="centro__content">
                    <div class="centro__titulos">
                        <h2><?php the_title();?></h2>
                        <h3>A mais avançada experiência de coaching no mundo.</h3>
                    </div>
                    <div class="centro__info">
                        <?php the_content();?>
                    </div>
            </div>
            <div class="centro__thumb">
                <?php the_post_thumbnail('full');?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="centro__lista">
            <?php //áreas
                $areas         = array(
                    'Livraria'              => 'livraria',
                    'Café'                  => 'cafe',
                    'Salas de Treinamento'  => 'treino',
                    'Sala para sessões'     => 'sessoes'
                );

                 foreach ($areas as $section => $area) {
                    $icone  = get_post_meta(get_the_id(), $area.'_image', 'true');
                    $desc   = get_post_meta(get_the_id(), $area.'_descricao', 'true');
                    $bloco  = '<div class="centro__bloco">';
                        $bloco .= '<div class="centro__icone">';
                            $bloco .= '<div class="centro__circle">';
                            $bloco .= wp_get_attachment_image( $icone, 'full');
                            $bloco .= '</div>';
                        $bloco .= '</div>';
                        $bloco .= '<h3 class="centro__area">'.$section.'</h3>';
                        $bloco .= '<div class="centro__desc">'.$desc.'</div>';
                    $bloco .= '</div>';
                    echo $bloco;
                 }
            ?>
        </div>1
    </div>
</section>
<?php endwhile; wp_reset_postdata(); ?>

<section class="museu">
    <div class="container">
        <h3 class="museu__titulos"></h3>
        <div class="content">

        </div>
    </div>
</section>

<?php get_template_part('templates/sobre','barra');?>
<?php get_template_part('templates/html','footer');?>