<?php get_template_part('templates/html','header');?>

<section class="agenda agenda--page">
    <div class="container">        
        <div>
        <?php
            $get_data   = date('Y-m-d');
            $get_mes    = date('m');
            $get_ano    = date("Y");
            // $args = array(
            //     'posts_per_page'=> -1,
            //     'post_type'  => 'agenda',
            //     'meta_key'   => 'agenda_data',
            //     'meta_query' => array(
            //         'relation' => 'AND',
            //         array(
            //             'relation' => 'OR',
            //             array(
            //                 'key'     => 'agenda_data_fim',
            //                 'value'   => $get_data,
            //                 'compare' => '>=',
            //             ),
            //             array(
            //                 'key'     => 'agenda_data',
            //                 'value'   => $get_data,
            //                 'compare' => '>=',
            //             )
            //         ),
            //         array(
            //             'key'     => 'agenda_tipo',
            //             'value'   => '2',
            //             'compare' => '==',
            //         ),
            //     ), 'orderby' => 'meta_value', 'order' => 'ASC'
            // );
            // $agenda = new WP_Query($args);
            function formata_data($start, $final){

                    $mes_abrev = array(
                        "1"   => "Jan",
                        "2"   => "Fev",
                        "3"   => "Mar",
                        "4"   => "Abr",
                        "5"   => "Mai",
                        "6"   => "Jun",
                        "7"   => "Jul",
                        "8"   => "Ago",
                        "9"   => "Set",
                        "10"   => "Out",
                        "11"   => "Nov",
                        "12"   => "Dez"
                    );

                    $dateStart = date_parse_from_format('Y-m-d', $start);
                    $dateFinal = date_parse_from_format('Y-m-d', $final);
                    $mes_start = $mes_abrev[$dateStart['month']];
                    $mes_final = $mes_abrev[$dateFinal['month']];

                    if($start == $final) {
                        echo $dateFinal['day'].' de '.
                             $mes_start.' '.
                             $dateFinal['year'];
                    }

                    else if($dateStart['month'] == $dateFinal['month']) {
                        echo $dateStart['day'].' a '.
                             $dateFinal['day'].' '.
                             $mes_start.' '.
                             $dateFinal['year'];
                    }

                    else {
                        if($dateStart['year'] != $dateFinal['year']) :
                            echo $dateStart['day']. ' de '.
                                 $mes_start.' de '.
                                 $dateStart['year'].' à '.
                                 $dateFinal['day'].' de '.
                                 $mes_final.' '.
                                 $dateFinal['year'];
                        else :
                             echo $dateStart['day']. ' de '.
                                 $mes_start.' à '.
                                 $dateFinal['day'].' de '.
                                 $mes_final.' '.
                                 $dateFinal['year'];
                        endif;
                    }
            }

            while (have_posts()) : the_post();
                $data  = get_post_meta( get_the_id(), 'agenda_data', true);
                $dataf = get_post_meta( get_the_id(), 'agenda_data_fim', true);
                $local = get_post_meta( get_the_id(), 'agenda_local', true);
                $curso = get_post_meta( get_the_id(), 'agenda_curso', true);
                $color = get_post_meta( $curso, 'curso_cor', true);
            ?>

            <div class="agenda__item">
                <div class="agenda__infos">
                    <div class="agenda__local">
                        <i style="color: <?php echo $color;?>;" class="fa fa-map-marker"></i><?php echo $local;?>
                    </div>
                    <div class="agenda__data">
                        <i style="color: <?php echo $color;?>;" class="fa fa-calendar-o"></i><?php formata_data($data, $dataf);?>
                    </div>
                    <div class="agenda__titulo"><?php echo get_the_title($curso);?></div>
                    <div class="agenda__btn">
                        <a href="<?php the_permalink();?>" class="btn" style="border-color: <?php echo $color;?>;">Saiba mais</a>
                    </div>
                </div>
                <div class="agenda__thumb">
                    <?php //echo $curso; ?>
                    <?php thumblazy($curso, 'full', 'fade', get_the_title($curso));?>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata();?>
        </div>
    </div>
</section>

<?php get_template_part('templates/frontpage','newsletter');?>
<?php get_template_part('templates/html','footer');?>