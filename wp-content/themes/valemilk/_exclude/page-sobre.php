<?php /* Template Name: Sobre a Febracis */ ?>
<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<section class="pages pages--sobre">
    <div class="container">
        <div class="pages__header">
            <h2 class="pages__headline">
                <?php the_title();?>
            </h2>
        </div>
        <h3 class="pages__titulo">Febracis. Mudando vidas a partir do coaching integral sistemico.</h3>
        <div class="pages__content">
            <div>
                <?php the_content(); ?>
            </div>
            <div class="pages__row pages__row--button">
                <a class="pages__btn" href="<?php echo get_post_type_archive_link('cursos');?>">Conhecer os cursos Febracis</a>
            </div>
        </div>

        <div class="pages__pilares pilares">
            <?php //áreas
            $pilares         = array(
                'Missão Febracis'  => 'missao',
                'Visão Febracis'   => 'visao',
                'Valores Febracis' => 'valores'
            );

            foreach ($pilares as $section => $pilar) {
                $icone  = get_post_meta(get_the_id(), $pilar.'_image', 'true');
                $desc   = get_post_meta(get_the_id(), $pilar.'_descricao', 'true');
                $bloco  = '<div class="pilares__bloco">';
                    $bloco .= '<div class="pilares__icone">';
                        $bloco .= '<div class="pilares__circle">';
                        $bloco .= wp_get_attachment_image( $icone, 'full');
                        $bloco .= '</div>';
                    $bloco .= '</div>';
                    $bloco .= '<h3 class="pilares__area">'.$section.'</h3>';
                    $bloco .= '<div class="pilares__desc">'.$desc.'</div>';
                $bloco .= '</div>';
                echo $bloco;
            }
            ?>
        </div>
        
    </div>
</section>
<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/sobre','pv');?>
<?php get_template_part('templates/sobre','livros');?>
<?php get_template_part('templates/sobre','news');?>
<?php get_template_part('templates/sobre','centro');?>
<?php get_template_part('templates/sobre','depoimentos');?>
<?php get_template_part('templates/sobre','social');?>
<?php get_template_part('templates/sobre','barra');?>
<?php get_template_part('templates/html','footer');?>