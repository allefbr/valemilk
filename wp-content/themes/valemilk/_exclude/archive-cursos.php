<?php get_template_part('templates/html','header');?>
<?php get_template_part('templates/cursos','news');?>

<?php
// $args = array(
//     'posts_per_page'=> -1,
//     'post_type'  => 'cursos',
//     'orderby' => 'meta_value', 'order' => 'ASC'
// );
// global $wp_query; query_posts( array_merge($wp_query->query, $args));
?>

<section class="curso curso--page">
    <div class="container">
        <div class="curso__head">
            <div class="curso__headline">Qual pilar da sua vida você quer mudar?</div>
        </div>
        <?php //MONTA SELECT
        $args_curso = array('taxonomy' => 'pilares', 'orderby' => 'title', 'order' => 'ASC', 'parent' => '0', 'hide_empty' => false);
        $cat_curso = get_categories($args_curso);
        if (!empty($cat_curso) && !is_wp_error($cat_curso)) : ?>
        <div class="curso__form">
            <form id="pilar" class="sform sform--dark" method="POST" onsubmit="return false;">
                <div class="sform__select sform__select--pilar">
                    <select name="pilar" class="cs-select cs-skin-elastic">
                    <option value="" disabled selected>Selecione um pilar</option>
                        <?php //options
                            foreach ( $cat_curso as $categoria ) {
                                echo '
                                <option value="'.$categoria->slug.'">'.
                                $categoria->name
                                .'</option>';
                            }
                        ?>
                    </select>
                </div>
                <input class="btn sform__submit" type="submit"  value="Buscar">
            </form>
        </div>
        <?php endif;?>

        <div class="curso__head">
            <div class="curso__categoria">Cursos Presenciais</div>
        </div>
        <div class="curso__blocos">
            <?php //presenciais
            $args = array(
                'posts_per_page'=> 6,
                'post_type'     => 'cursos',
                'orderby'       => 'date',
                'order'         => 'DESC',
                'tax_query' => array(
                    array(
                    'taxonomy'         => 'tipos',
                    'field'            => 'id',
                    'terms'            => array(18),
                    'operator'         => 'IN'
                    )
                ),
            );
            $presencial = new WP_Query($args);
            $excluir = array();
            while ($presencial->have_posts()) : $presencial->the_post();
                $excluir[] = get_the_id();
                get_template_part('templates/loop','cursos');
            endwhile; wp_reset_postdata();?>
        </div>
        <div class="curso__head">
            <div class="curso__categoria">Cursos Online</div>
        </div>
        <div class="curso__blocos">
            <?php //online
            // print_r($excluir);
            $args2 = array(
                'posts_per_page' => 6,
                'post_type'      => 'cursos',
                'orderby'        => 'date',
                'order'          => 'DESC',
                'tax_query' => array(
                    array(
                    'taxonomy'         => 'tipos',
                    'field'            => 'id',
                    'terms'            => array(19),
                    'operator'         => 'IN'
                    )
                ),
                'post__not_in'   => array($excluir),
            );
            $online = new WP_Query($args2);
            while ($online->have_posts()) : $online->the_post();
                get_template_part('templates/loop','cursos');
            endwhile; wp_reset_postdata();?>
        </div>
    </div>
</section>

<?php get_template_part('templates/frontpage','newsletter');?>
<?php get_template_part('templates/html','footer');?>