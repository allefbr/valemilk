<?php /* Template Name: Coach Social */ ?>
<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<?php   //Custom Fields
        $coachvida       = get_post_meta( get_the_id(), 'vida_descricao', 'true');
        $coachvida_gal   = get_post_meta( get_the_id(), 'vida_images', 'true');
        $coachsocial     = get_post_meta( get_the_id(), 'social_descricao', 'true');
        $coachsocial_gal = get_post_meta( get_the_id(), 'social_images', 'true');
        $coluna1         = get_post_meta( get_the_id(), 'coluna1_descricao', 'true');
        $imagem1         = get_post_meta( get_the_id(), 'coluna1_image', 'true');
        $coluna2         = get_post_meta( get_the_id(), 'coluna2_descricao', 'true');
        $imagem2         = get_post_meta( get_the_id(), 'coluna2_image', 'true');
?>
<section class="pages pages--social">
    <div class="container">
        <div class="pages__content">
            <div class="pages__bloco">
                <div class="pages__coluna">
                    <?php echo $coluna1;?>
                </div>
                <div class="pages__coluna pages__coluna--destaque">
                    <?php echo get_the_content();?>
                </div>
            </div>
            <div class="pages__bloco">
                <div class="pages__coluna">
                    <?php echo wp_get_attachment_image( $imagem1, 'full');?>
                </div>
                <div class="pages__coluna">
                    <?php echo $coluna2;?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>