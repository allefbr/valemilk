<?php /* Template Name: Monitoria Febracis */ ?>
<?php get_template_part('templates/html','header');?>
<?php while (have_posts()) : the_post(); ?>
<?php //Custom Fields ?>
<section class="pages pages--monitoria">
    <div class="container">
        <div class="pages__content">
            <?php the_content();?>
        </div>
    </div>
</section>
<section class="monitoria">
    <div class="container">
        <div class="monitoria__galeria">
            <?php $galeria = rwmb_meta('monitoria_galeria','size=full');

                print_r($galeria);
                // $images = rwmb_meta( 'gallery', 'size=YOURSIZE' );            // Since 4.8.0
                // $images = rwmb_meta( 'gallery', 'type=image&size=YOURSIZE' ); // Prior to 4.8.0
                // if ( !empty( $images ) ) {
                //     foreach ( $images as $image ) {
                //     echo "<a href='{$image['full_url']}' rel='lightbox'><img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' /></a>";
                //     }
                // }

                // if($galeria) :
                    foreach ($galeria as $item) {
                        echo '
                        <div class="monitoria__item">
                            <div class="monitoria__thumb">';
                                imglazy($item['icone'][0], 'full', 'fade', $item['titulo']);
                            echo '</div>
                        </div>';
                    }
                // endif;
            ?>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_postdata(); ?>
<?php get_template_part('templates/html','footer');?>