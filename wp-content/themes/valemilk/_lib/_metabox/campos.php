<?php

$prefix = 'wpcf_';
add_filter( 'rwmb_meta_boxes', 'wpcf_meta_boxes' );
function wpcf_meta_boxes($meta_boxes) {

//==============================================
// GERAL - OPTION LINKS
//==============================================

$meta_boxes[] = array(

	'id' => 'links',
	'title' => 'Opções do Link',
	'pages' => array( 'news', 'livros'),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'   	 => 'Link',
			'id'	 	 => "opt_link",
			'desc'		 => 'Defina o Link do Slide',
			'type'	 	 => 'text',
			'admin_columns' => array('position' => 'after title', 'title' => 'URL', 'sort' => false),
		),
		array(
			'name'   	 => 'Target do Link',
			'id'	 	 => "opt_target",
			'type'	 	 => 'radio',
			'options' 	 => array('_self' => 'Interno', '_blank' => 'Externo'),
			'std' 		 => '_self',
			'admin_columns' => array('position' => 'after title', 'title' => 'Tipo de Link', 'sort' => true),
		),
		array(
			'name'   	 => 'Texto Botão',
			'id'	 	 => "opt_text",
			'desc'		 => 'Defina o texto do botão',
			'std'		 => 'Saiba Mais',
			'type'	 	 => 'text',
		),
	)
);

//==============================================
// AGENDA
//==============================================

$meta_boxes[] = array(

	'id' => 'agenda',
	'title' => 'Detalhes do Evento',
	'pages' => array( 'agenda' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'   	 => 'Tipo do Evento',
			'id'	 	 => "agenda_tipo",
			'type'	 	 => 'radio',
			'std'		 => '1',
			'options' 	 => array('1' => 'Evento', '2' => 'Curso'),
		),
		array(
			'name'        => 'Curso',
			'id'          => 'agenda_curso',
			'type'        => 'post',
			// Post type
			'post_type'   => 'cursos',
			// Field type, either 'select' or 'select_advanced' (default)
			'field_type'  => 'select_advanced',
			'placeholder' => 'Selecione um curso',
			// Query arguments (optional). No settings means get all published posts
			'query_args'  => array(
				'post_status'    => 'publish',
				'posts_per_page' => - 1,
			),
			'hidden'	 => array( 'agenda_tipo', '!=', '2'),
		),
		array(
			'name'   	 	=> 'Data',
			'id'	 	 	=> "agenda_data",
			'desc'			=> 'Defina a data do evento',
			'type'	 	 	=> 'date',
			'format' 	 	=> 'yy-mm-dd',
			'required'   	=> true,
			// 'admin_columns' => array('position' => 'after title','title' => 'Dia do Show', 'sort' => true),
		),
		array(
			'name'   	 	=> 'Data',
			'id'	 	 	=> "agenda_data_fim",
			'desc'			=> 'Defina a data do evento',
			'type'	 	 	=> 'date',
			'format' 	 	=> 'yy-mm-dd',
			'required'   	=> true,
			// 'admin_columns' => array('position' => 'after title','title' => 'Dia do Show', 'sort' => true),
		),
		array(
			'name'  		=> 'Local',
			'id'			=> "agenda_local",
			'type'			=> 'text',
			'required'  	=> true,
			'desc'			=> 'Defina a cidade do evento. Ex: Fortaleza, CE',
			'admin_columns' => array('position' => 'before date', 'title' => 'Local'),
		),
		array(
			'name'   	 => 'Ano',
			'id'	 	 => "agenda_ano",
			'type'	 	 => 'hidden',
			// 'admin_columns' => array('position' => 'before date', 'title' => 'Ano'),
		),
		array(
			'name'   	 => 'Mês',
			'id'	 	 => "agenda_mes",
			'type'	 	 => 'hidden',
			// 'admin_columns' => array('position' => 'before date', 'title' => 'Mes'),
		),
	)
);

//==============================================
// NEWS
//==============================================

$meta_boxes[] = array(

	'id' => 'news',
	'title' => 'Posicão no tema',
	'pages' => array( 'news' ),
	'context' => 'side',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name' => 'Frontpage',
			'id'   => 'news_posicao',
			'type'    => 'checkbox_list',
			// Options of checkboxes, in format 'value' => 'Label'
			'options' => array(
				'0' => 'Header News',
				'1' => 'Frontpage News',
				'2' => 'Frontpage Bloco',
				'3' => 'Frontpage Full 1',
				'4' => 'Frontpage Full 2',
				'5' => 'Sobre News',
				'7' => 'Sobre Barra',
				'6' => 'Cursos',
			),
			'admin_columns' => array('position' => 'after title', 'title' => 'Posição', 'sort' => true),
		),
		array(
			'name'   	 		=> 'Add Label "Novo"?',
			'id'	 	 		=> "news_label",
			'type'	 	 		=> 'radio',
			'options' 	 		=> array('1' => 'Sim', '0' => 'Não'),
			'std' 		 		=> '0',
			'visible' 			=> array('news_posicao', 'in', '0'),
		),
		array(
			'name'   	 		=> 'Background Imagem',
			'id'	 	 		=> "news_background",
			'desc'		 		=> 'Defina a imagem de background',
			'type'       	    => 'image_advanced',
			'max_file_uploads'  => 1,
			'visible' 			=> array('news_posicao', 'in', '3, 4'),
		),
	)
);

//==============================================
// LIVROS
//==============================================

$meta_boxes[] = array(

	'id' => 'books',
	'title' => 'Posicão no tema',
	'pages' => array( 'livros' ),
	'context' => 'side',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name' => 'Posição',
			'id'   => 'books_posicao',
			'type'    => 'checkbox_list',
			// Options of checkboxes, in format 'value' => 'Label'
			'options' => array(
				'1' => 'E-book',
				'2' => 'Paulo Vieira',
				'5' => 'Página Sobre',
				'4' => 'Página Centro Conceito',
				'3' => 'Rodapé',
			),
			'admin_columns' => array('position' => 'after title', 'title' => 'Posição', 'sort' => true
				),
		),
	)
);

//==============================================
// CORES
//==============================================
$meta_boxes[] = array(
	'id' => 'cores',
	'title' => 'Cor do Label',
	'pages' => array( 'cursos', 'agenda', 'news'),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name' => 'Label',
			'id'   => 'opt_label',
			'type' => 'text',
		),
		array(
			'name' => 'Cor',
			'id'   => 'opt_cor',
			'type' => 'color',
		),
	)
);

//==============================================
// NOTICIAS
//==============================================

$meta_boxes[] = array(

	'id' => 'cursos',
	'title' => 'Destacar Curso',
	'pages' => array( 'cursos' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'   	 => 'Destacar',
			'id'	 	 => "curso_destaque",
			'type'	 	 => 'radio',
			'options' 	 => array('1' => 'Sim', '0' => "Não"),
			'admin_columns' => array('position' => 'after title', 'title' => 'Destaque da Categoria', 'sort' => true),
		),
	)
);

//==============================================
// SLIDES
//==============================================

$meta_boxes[] = array(

	'id' => 'slides',
	'title' => 'Detalhes do Slide',
	'pages' => array( 'slides' ),
	'context' => 'normal',
	'priority' => 'high',

	'fields' => array(
		array(
			'name'   	 => 'Descrição',
			'id'	 	 => "slide_desc",
			'desc'		 => 'Descrição',
			'type'	 	 => 'wysiwyg',
		),

		array(
			'name'   	 => 'Subtitulo',
			'id'	 	 => "slide_subtitle",
			'desc'		 => 'Subtitulo',
			'type'	 	 => 'text',
		),

		array(
			'name'   	 => 'Titulo Thumbnail',
			'id'	 	 => "slide_title_thumb",
			'desc'		 => 'Titulo da Thumb',
			'type'	 	 => 'text',
		),

		array(
			'name'   	 => 'Link',
			'id'	 	 => "slide_link",
			'desc'		 => 'Defina o Link do Slide',
			'type'	 	 => 'text',
		),

		array(
			'name'   	 => 'Target do Link',
			'id'	 	 => "slide_target",
			'type'	 	 => 'radio',
			'options' 	 => array('_selfie' => 'Interno', '_blank' => 'Externo'),
			'std' 		 => '_selfie'
		),

		array(
			'name'   	 => 'Texto Botão',
			'id'	 	 => "slide_btn",
			'desc'		 => 'Defina o texto do botão',
			'type'	 	 => 'text',
		),
	)
);

//==============================================
// HEADER
//==============================================

$optionsArchives = array (
    'agenda' => 'Agenda',
    'cursos' => 'Cursos'
);

$meta_boxes[] = array(

	'id' => 'headers',
	'title' => 'Detalhes do Slide',
	'pages' => array( 'header' ),
	'context' => 'side',
	'priority' => 'high',

	'fields' => array(

		array(
			'name'     	  => 'Archives',
			'id'       	  => $prefix . 'archives',
			'type'        => 'select_advanced',
			'options'  	  => $optionsArchives,
			'multiple'    => true,
			'placeholder' => 'Selecione os archives',
			'admin_columns' => array('position' => 'after title', 'title' => 'Archives', 'sort' => false),
		),

		array(
			'name'        => 'Páginas',
			'id'          => $prefix.'pages',
			'type'        => 'post',
			'post_type'   => array('page'),
			'field_type'  => 'select_advanced',
			'multiple'    => true,
			'placeholder' => 'Selecione uma página',
			'query_args'  => array(
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'orderby'		 => 'title',
				'order'			 => ASC
			),
			'admin_columns' => array('position' => 'after title', 'title' => 'Pages', 'sort' => false),
		),

		array(
			'name'       => 'WP Default',
			'id'         => $prefix . 'sistema',
			'type'    	 => 'checkbox_list',
			'options' 	 => array('search' => 'Pesquisa','category' => 'Categorias', 'midias' => 'Imprensa'),
			'admin_columns' => array('position' => 'after title', 'title' => 'Sistema', 'sort' => false),
		),

		array(
			'name'       => 'Vídeo',
			'id'         => $prefix . 'video',
			'type'    	 => 'radio',
			'options' 	 => array('1' => 'Sim', '0' => 'Não'),
			'std'		 => '0',
			'admin_columns' => array('position' => 'after title', 'title' => 'Vídeo', 'sort' => true),
		),

		array(
			'name'       => 'ID Vídeo Youtube',
			'id'         => $prefix . 'video_id',
			'type'    	 => 'text',
			'visible' 	 => array('video', '=', '1')
		),

		// array(
		// 	'name'        => 'Posts',
		// 	'id'          => $prefix .'posts',
		// 	'type'        => 'post',
		// 	'post_type'   => array('post'),
		// 	'field_type'  => 'select_advanced',
		// 	'multiple'    => true,
		// 	'placeholder' => 'Selecione um Post',
		// 	'query_args'  => array(
		// 		'post_status'    => 'publish',
		// 		'posts_per_page' => -1,
		// 		'oder_by'		 => 'title',
		// 		'order'			 => ASC
		// 	),
		// ),
	)
);

//=========================================================================================
// END DEFINITION OF META BOXES
//=========================================================================================
	return $meta_boxes;
}