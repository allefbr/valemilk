<?php

add_filter( 'rwmb_meta_boxes', 'wpcf_meta_boxes_pages' );
function wpcf_meta_boxes_pages($meta_boxes) {

//=========================================================================================
// PAGES: SOBRE
//=========================================================================================

$meta_boxes[] = array(
    'id' => 'sobre_campos',
    'title' => 'Outras informações',
    'pages' => array('page'),
    'context' => 'normal',
    'priority' => 'high',

    // 'include' => array(
    //     'relation'  => 'OR',
    //     'template'  => 'page-institucional.php',
    // ),

    'tabs' => array(
        'missao' => array(
            'label' => 'Missão',
            'icon'  => 'dashicons-info',
        ),
        'visao' => array(
            'label' => 'Visão',
            'icon'  => 'dashicons-info',
        ),
        'valores' => array(
            'label' => 'Valores',
            'icon'  => 'dashicons-info',
        ),
    ),
    // Tab style: 'default', 'box' or 'left'. Optional
    'tab_style' => 'left',
    // Show meta box wrapper around tabs? true (default) or false. Optional
    'tab_wrapper' => true,
    // List of meta fields
    'fields' => array(

        //MISSÃO
        array(
            'name'              => 'Icone',
            'id'                => "missao_image",
            'type'              => 'image_advanced',
            'max_file_uploads'  => 1,
            'tab'               => 'missao',
        ),
        array(
            'name'      => 'Conteúdo',
            'id'        => $prefix . 'missao_descricao',
            'type'      => 'wysiwyg',
            'tab'       => 'missao',
            'options' => array(
                'textarea_rows' => 7,
                'teeny'         => true,
                'media_buttons' => false,
            ),
        ),

        //VISÃO
        array(
            'name'              => 'Icone',
            'id'                => "visao_image",
            'type'              => 'image_advanced',
            'max_file_uploads'  => 1,
            'tab'               => 'visao',
        ),

        array(
            'name'      => 'Descrição',
            'id'        => $prefix . 'visao_descricao',
            'type'      => 'wysiwyg',
            'tab'       => 'visao',
            'options'   => array(
                'textarea_rows' => 7,
                'teeny'         => true,
                'media_buttons' => false,
            ),
        ),

        //VALORES
        array(
            'name'              => 'Icone',
            'id'                => "valores_image",
            'type'              => 'image_advanced',
            'max_file_uploads'  => 1,
            'tab'               => 'valores',
        ),

        array(
            'name'      => 'Descrição',
            'id'        => $prefix . 'valores_descricao',
            'type'      => 'wysiwyg',
            'tab'       => 'valores',
            'options'   => array(
                'textarea_rows' => 7,
                'teeny'         => true,
                'media_buttons' => false,
            ),
        ),
    )
);


//=========================================================================================
// END DEFINITION OF META BOXES
//=========================================================================================
    return $meta_boxes;
}