<?php

add_filter( 'mb_settings_pages', 'prefix_settings_pages' );
function prefix_settings_pages( $settings_pages ) {
    $settings_pages[] = array(
        'id'            => 'general',
        'option_name'   => 'options_general',
        'menu_title'    => 'Opções do Site',
        'icon_url'      => 'dashicons-images-alt',
    );
    return $settings_pages;
}

add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes ) {

    $meta_boxes[] = array(
        'id'             => 'folder',
        'title'          => 'Slide Home: Vídeo e Imagem',
        'settings_pages' => 'general',
        'context'        => 'normal',
        'fields'         => array(
            array(
                'name' => '',
                'id'   => 'option_video',
                'type' => 'file_advanced',
                'max_file_uploads' => 1,
            ),
            array(
                'name' => '',
                'id'   => 'option_image',
                'type' => 'file_advanced',
                'desc' => 'Defina uma imagem para substituir o vídeo nos dispositivos móveis',
                'max_file_uploads' => 1,
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'             => 'links',
        'title'          => 'Link Loja',
        'context'        => 'side',
        'settings_pages' => 'general',
        'fields'         => array(
            array(
                'name' => 'Loja',
                'id'   => 'option_loja',
                'type' => 'text',
            )
        ),
    );
    return $meta_boxes;
}