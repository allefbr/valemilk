<?php
/*
* Configurações de Taxonomias
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// MODELO
//=========================================================================================

// $labels = array( 'name' => _x( 'NOME DA TAXONOMIA', 'Taxonomy General Name', 'text_domain' ));
// $args   = array(
//   'labels'            => $labels,
//   'hierarchical'      => true,
//   'public'            => true,
//   'show_ui'           => true,
//   'show_admin_column' => true,
//   'show_in_nav_menus' => true,
//   'show_tagcloud'     => true,
//   'rewrite'           => array('slug' => 'SLUG DA TAX'),
// );

// register_taxonomy( 'TAXONOMIA', array('TIPO DE POST OU POST TYPE'), $args);

//=========================================================================================
// TAXONOMIA TIPO DO ARQUIVO IMPRENSA
//=========================================================================================

// $labels = array( 'name' => _x( 'Tags', 'Taxonomy General Name', 'text_domain' ));
// $args   = array(
//   'labels'            => $labels,
//   'hierarchical'      => true,
//   'public'            => true,
//   'show_ui'           => true,
//   'show_admin_column' => true,
//   'show_in_nav_menus' => true,
//   'show_tagcloud'     => true,
//   'rewrite'           => array('slug' => 'news'),
// );

// register_taxonomy( 'news', array( 'news' ), $args );

//=========================================================================================
// TAXONOMIAS AGENDA
//=========================================================================================

$labels = array( 'name' => _x( 'Estado', 'Taxonomy General Name', 'text_domain' ));
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'agenda/estado'),
);

register_taxonomy( 'eventos', array( 'agenda' ), $args );

$labels = array( 'name' => _x( 'Formação', 'Taxonomy General Name', 'text_domain' ));
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'agenda/formacao'),
);

register_taxonomy( 'formacao', array( 'agenda' ), $args );

//=========================================================================================
// TAXONOMIAS CURSOS
//=========================================================================================

$labels = array( 'name' => _x( 'Pilares', 'Taxonomy General Name', 'text_domain' ));
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'pilar'),
);

register_taxonomy( 'pilares', array( 'cursos' ), $args );

$labels = array( 'name' => _x( 'Tipo de Curso', 'Cursos', 'text_domain' ));
$args   = array(
  'labels'            => $labels,
  'hierarchical'      => true,
  'public'            => true,
  'show_ui'           => true,
  'show_admin_column' => true,
  'show_in_nav_menus' => true,
  'show_tagcloud'     => true,
  'rewrite'           => array('slug' => 'curso'),
);

register_taxonomy( 'tipos', array( 'cursos' ), $args );

//=========================================================================================
// FLUSH REWRITE
//=========================================================================================

function custom_taxonomy_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_action('init', 'custom_taxonomy_flush_rewrite');