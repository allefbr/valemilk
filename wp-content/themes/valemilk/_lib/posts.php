<?php
/**
* Custom Post Types
* Desenvolvedor: Nicholas Lima
* Email: nick.lima.wp@gmail.com
*/

//=========================================================================================
// POST TYPE AGENDA
//=========================================================================================

// function post_type_agenda_register() {
//     $labels = array(
//         'name' => 'Agenda',
//         'singular_name' => 'Agenda',
//         'menu_name' => 'Agenda',
//         'add_new' => _x('Adicionar Evento', 'item'),
//         'add_new_item' => __('Adicionar Novo Evento'),
//         'edit_item' => __('Editar Evento'),
//         'new_item' => __('Novo Evento')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => true,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'agenda'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-calendar',
//         'supports' => array('title','editor', 'thumbnail')
//     );
//     register_post_type('agenda', $args);
// }
// add_action('init', 'post_type_agenda_register');

// //=========================================================================================
// // POST TYPE CURSOS
// //=========================================================================================

// function post_type_cursos_register() {
//     $labels = array(
//         'name' => 'Curso',
//         'singular_name' => 'Curso',
//         'menu_name' => 'Cursos',
//         'add_new' => _x('Adicionar Curso', 'item'),
//         'add_new_item' => __('Adicionar Novo Curso'),
//         'edit_item' => __('Editar Curso'),
//         'new_item' => __('Novo Curso')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => true,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'cursos'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-welcome-learn-more',
//         'supports' => array('title','editor', 'thumbnail')
//     );
//     register_post_type('cursos', $args);
// }
// add_action('init', 'post_type_cursos_register');

// //=========================================================================================
// // IMPRENSA
// //=========================================================================================

// add_action('init', 'post_type_imprensa_register');
// function post_type_imprensa_register() {
//     $labels = array(
//         'name' => 'Imprensa',
//         'singular_name' => 'Imprensa',
//         'menu_name' => 'Imprensa',
//         'add_new' => _x('Adicionar Arquivo', 'item'),
//         'add_new_item' => __('Adicionar Arquivo'),
//         'edit_item' => __('Editar Arquivo'),
//         'new_item' => __('Nova Arquivo')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => true,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'imprensa'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-lock',
//         'supports' => array('title', 'editor', 'thumbnail')
//     );

//     register_post_type('imprensa', $args);
// }

// //=========================================================================================
// // EQUIPE
// //=========================================================================================

// function post_type_equipe_register() {
//     $labels = array(
//         'name' => 'Equipe',
//         'singular_name' => 'Equipe',
//         'menu_name' => 'Equipe',
//         'add_new' => _x('Adicionar Membro', 'item'),
//         'add_new_item' => __('Adicionar Novo Membro'),
//         'edit_item' => __('Editar Membro'),
//         'new_item' => __('Nova Membro')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => false,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'equipe'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-awards',
//         'supports' => array('title','thumbnail')
//     );
//     register_post_type('equipe', $args);
// }
// add_action('init', 'post_type_equipe_register');

// //=========================================================================================
// // POST TYPE NEWS
// //=========================================================================================

// function post_type_news_register() {
//     $labels = array(
//         'name' => 'Destaques',
//         'singular_name' => 'Destaque',
//         'menu_name' => 'Destaques',
//         'add_new' => _x('Adicionar Destaque', 'item'),
//         'add_new_item' => __('Adicionar Novo Destaque'),
//         'edit_item' => __('Editar Destaque'),
//         'new_item' => __('Novo Destaque')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => false,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'news'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-lightbulb',
//         'supports' => array('title','editor', 'thumbnail')
//     );
//     register_post_type('news', $args);
// }
// add_action('init', 'post_type_news_register');

// //=========================================================================================
// // POST TYPE QUOTES
// //=========================================================================================

// function post_type_quote_register() {
//     $labels = array(
//         'name' => 'Depoimentos',
//         'singular_name' => 'Depoimento',
//         'menu_name' => 'Depoimentos',
//         'add_new' => _x('Adicionar Depoimento', 'item'),
//         'add_new_item' => __('Adicionar Novo Depoimento'),
//         'edit_item' => __('Editar Depoimento'),
//         'new_item' => __('Nova Depoimento')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => true,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'depoimentos'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-format-quote',
//         'supports' => array('title','thumbnail','editor')
//     );
//     register_post_type('quotes', $args);
// }
// add_action('init', 'post_type_quote_register');

// //=========================================================================================
// // POST TYPE LIVROS
// //=========================================================================================

// function post_type_livro_register() {
//     $labels = array(
//         'name' => 'Livros',
//         'singular_name' => 'Livro',
//         'menu_name' => 'Livros',
//         'add_new' => _x('Adicionar Livro', 'item'),
//         'add_new_item' => __('Adicionar Novo Livro'),
//         'edit_item' => __('Editar Livro'),
//         'new_item' => __('Nova Livro')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => false,
//         'publicly_queryable' => true,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'livros'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-book',
//         'supports' => array('title','editor', 'thumbnail')
//     );
//     register_post_type('livros', $args);
// }
// add_action('init', 'post_type_livro_register');

// //=========================================================================================
// // POST TYPE SLIDES
// //=========================================================================================

// function post_type_slide_register() {
//     $labels = array(
//         'name' => 'Slides',
//         'singular_name' => 'Slide',
//         'menu_name' => 'Slides',
//         'add_new' => _x('Adicionar Slide', 'item'),
//         'add_new_item' => __('Adicionar Novo Slide'),
//         'edit_item' => __('Editar Slide'),
//         'new_item' => __('Novo Slide')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => false,
//         'publicly_queryable' => false,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'slides'),
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-images-alt2',
//         'supports' => array('title', 'thumbnail')
//     );
//     register_post_type('slides', $args);
// }
// add_action('init', 'post_type_slide_register');

// //=========================================================================================
// // POST TYPE HEADER
// //=========================================================================================

// function post_type_header_register() {
//     $labels = array(
//         'name' => 'Capas',
//         'singular_name' => 'Capa',
//         'menu_name' => 'Capas',
//         'add_new' => _x('Adicionar Capa', 'item'),
//         'add_new_item' => __('Adicionar Nova Capa'),
//         'edit_item' => __('Editar Capa'),
//         'new_item' => __('Nova Capa')
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => false,
//         'publicly_queryable' => false,
//         'show_ui' => true,
//         'show_in_menu' => true,
//         'query_var' => true,
//         'rewrite' => array('slug' => 'header'),
//         'capability_type' => 'post',
//         'has_archive' => false,
//         'hierarchical' => true,
//         'menu_position' => 5,
//         'menu_icon' => 'dashicons-images-alt',
//         'supports' => array('title', 'editor', 'thumbnail')
//     );
//     register_post_type('header', $args);
// }
// add_action('init', 'post_type_header_register');