<?php get_template_part('templates/html','header');?>

<section class="painel">	
	<ul class="slide">
		<li class="slide__item">
			<div class="slide__thumbs">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide1.jpg" alt="Imagem do Painel" />
			</div>
			
			<div class="container">
				<div class="slide__desc">
					
					<h2 class="slide__tit">Qualide e sabores diferenciados.</h2>
					<div class="slide_desc">
						<p>Produtos feitos com cuidado e pensados para trazer o melhor no seu dia a dia</p>
					</div>

					<a href="#" class="slide__btn">Conhceça mais</a>
				</div>
			</div>
		</li>
	</ul>
</section>




<?php get_template_part('templates/html','footer');?>