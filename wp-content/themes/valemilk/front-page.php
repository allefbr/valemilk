<?php get_template_part('templates/html','header');?>

<section class="section painel">	
	<ul id="slide" class="slide">
		<li class="slide__item">
			<div class="slide__thumbs">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide1.jpg" alt="Imagem do Painel" />
			</div>
			
			<div class="container">
				<div class="slide__infos">
					
					<h2 class="slide__tit">Qualide e sabores diferenciados.</h2>
					<div class="slide__desc">
						<p>Produtos feitos com cuidado e pensados para trazer o melhor no seu dia a dia</p>
					</div>

					<a href="#" class="slide__btn">Conheça mais</a>
				</div>
			</div>
		</li>
	</ul>
</section>

<section class="section sobre">	
	<span class="icon-center"><i class="icon icon-icon-vaca"></i></span>

	<div class="container">
		<div class="col-half">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-sobre.jpg" alt="Imagem institucional" class="img-radius" />
		</div>

		<div class="col-half">
			<h2 class="sobre__tit">Laticínios Valemilk</h2>

			<div class="sobre__desc">
				<p>A nossa história começou em 1998 no município de Pentecoste – CE, com a criação de uma pequena agroindústria para o beneficiamento de leite.</p>

				<p>hoje o laticínio Valemilk oferece ao mercado um variado mix de produtos lácteos com alto padrão de qualidade e sabores diferenciados.</p>
			</div>

			<a href="#" class="sobre__btn">Conheça mais</a>
		</div>
	</div>
</section>


<section class="section produtos">
	<div class="container">
		
		<div class="headerTitle">
			<h2 class="headerTitle__tit">Produtos</h2>
			<div class="headerTitle__desc"><p>Produtos feitos com cuidado e pensados para trazer o melhor no seu dia a dia</p></div>
		</div>
		
		<ul id="prod-carousel" class="list-produtos">
			<li class="list-produtos__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/prod1.png" alt="">
			</li>

			<li class="list-produtos__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/prod2.png" alt="">
			</li>

			<li class="list-produtos__item">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/prod3.png" alt="">
			</li>
		</ul>

		<a href="#" class="produtos__btn">Conheça todos os produtos</a>
	</div>
</section>

<section class="section receitas">
	<div class="container">
				
		<div class="receita">			
			<div class="receita__tag">
				<strong class="receita__tag__tit">Pronto em</strong>
				<span class="receita__tag__min">40</span>
				<span class="receita__tag__text">mins</span>
			</div>

			<div class="receita__thumbs">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/receita1.jpg" alt="Imagem receita" class="receita__img" />
			</div>

			<div class="receita__infos">
				<h2 class="receita__tit">Fondue de queijo</h2>
				<h3 class="receita__subtit">Delicioso para aquele jantar a dois</h3>

				<div class="receita__desc">
					<p>Rale os queijos no ralador ou processador. Dissolva o amido de milho no vinho branco e misture com o conhaque. Adicione a noz-moscada a este líquido
Passe o alho nos lados e fundo da panela...</p>
				</div>
				<a href="#" class="receita__btn">Continue lendo ></a>
			</div>
		</div>
	

	</div>
</section>


<section class="section onde">
	<div class="container">
		<div class="onde__infos">

			<div class="onde__infos__icon">
				<i class="icon icon-marker"></i>
			</div>
			
			<div class="onde__infos__box">
				<h2 class="onde__tit">Onde encontrar PRODUTOS VALEMILK?</h2>
				<div class="onde__desc">
					<p>Acesse e descubra o ponto de venda mais proximo de você</p>
				</div>
				<a href="#" class="onde__btn">Veja o mapa</a>
			</div>
		</div>

		<div class="onde__thumb">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bg-localidade.jpg" alt="Imagem institucional" class="img-radius" />
		</div>
	</div>
</section>


<section class="section cta-prod">
	<div class="container">

		<div class="cta-prod__infos">
			<h2 class="cta-prod__tit">Quer produtos valemilk na sua loja?</h2>		
		</div>

		<div class="cta-prod__boxBtn">
			<a href="#" class="cta-prod__btn">Fale conosco</a>
		</div>

		<div class="cta-prod__thumbs">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/produtos.png" alt="Imagem dos produtos">
		</div>

	</div>
</section>

<?php get_template_part('templates/html','footer');?>