/*=========================================================================================
// INICIO MAIN JS
========================================================================================= */

// ;(function() {
//   [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
//     new SelectFx(el);
//   } );
// })();

jQuery(function($) {
    $(document).ready(function() {

  /*=========================================================================================
  // MENU TOOGLE
  =========================================================================================*/

// $('.menu-resp').on('click', function(e) {
//   e.preventDefault();

//   if($('.menu-resp').hasClass('down')) {
//     $('.menu-resp').removeClass('down');
//     $('.menu-resp').addClass('up');
//   }

//   else {
//     $('.menu-resp').removeClass('up');
//     $('.menu-resp').addClass('down');
//   }

//   $('.topo .wrap-menu').slideToggle();
// });

// $('.rdp__menu_resp').on('click', function(e) {
//   e.preventDefault();
//   $('.rdp__menu').slideToggle();
// });

$("#slide").owlCarousel({
  items: 1,
  nav: true,
  dots: false,
  navText: false,
  margin: 0,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn'
});

$("#prod-carousel").owlCarousel({
  items: 3,
  nav: true,
  center: true,
  dots: false,
  loop: true,
  navText: false,
});


/*=========================================================================================
// OWL
========================================================================================= */

// // Calendário
// var $sync1 = $("#owl-slides");
// var $sync2 = $("#owl-thumbs");
// var flag = false;
// var duration = 300;

// $sync1.owlCarousel({
//   items: 1,
//   nav: true,
//   dots: false,
//   margin: 0,
//   animateOut: 'fadeOut',
//   animateIn: 'fadeIn'
// }).on('changed.owl.carousel', function (e) {
//   if (!flag) {
//     flag = true;
//     $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
//     $sync2.find(".owl-item").removeClass("synced").eq(e.item.index).addClass("synced");
//     flag = false;
//   }
// });

// $sync2.owlCarousel({
//   margin: 0,
//   items: 5,
//   nav: false,
//   center: false,
//   dots: false,
//   mouseDrag: false,
//   responsive:{
//     0:{
//         items: 1,
//         dots: true,
//         loop: true
//     },
//     600:{
//         items: 3,
//         dots: false,
//         loop: false
//     },
//     1100:{
//         items: 5,
//         dots: false,
//         loop: false
//     }
//   }
// }).on('click', '.owl-item', function () {
//   $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
// }).on('changed.owl.carousel', function (e) {
//   if (!flag) {
//     flag = true;
//     $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
//     flag = false;
//   }
// });

// $sync2.find(".owl-item").eq(0).addClass("synced");

/*=========================================================================================
// LAZY
=========================================================================================*/

// var bLazy = new Blazy({
//     offset: 0, // Loads images 100px before they're visible
//     error: function(ele){
//         var original = ele.getAttribute('data-src');
//         ele.src = original;
//     }
// });

// function init() {
//   var imgDefer = document.getElementsByClassName('img-defer');
//   for (var i=0; i<imgDefer.length; i++) {
//     if(imgDefer[i].getAttribute('data-src')) {
//       imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
//     }
//   }
// }

// window.onload = init;

/*=========================================================================================
// EQUAL HEIGHT
=========================================================================================*/
// function equalizeHeights(selector) {
//   var heights = new Array();

//   // Loop to get all element heights
//   $(selector).each(function() {

//     // Need to let sizes be whatever they want so no overflow on resize
//     $(this).css('min-height', '0');
//     $(this).css('max-height', 'none');
//     $(this).css('height', 'auto');

//     // Then add size (no units) to array
//     heights.push($(this).height());
//   });

//   // Find max height of all elements
//   var max = Math.max.apply( Math, heights );

//   // Set all heights to max height
//   $(selector).each(function() {
//     $(this).css('height', max + 'px');
//   });
// }

// $(window).on('load resize', function(){
//     equalizeHeights('.news--sobre .news__infos');
//     equalizeHeights('.curso__desc');
//     equalizeHeights('.sistema__item');
//     equalizeHeights('.titulo');
// });

/*=========================================================================================
// CLOSE FUNCTION
=========================================================================================*/
    });
});