</main>
<footer class="rodape">   
	<div class="rodape__menu">
		<div class="container">
		   <?php
		    if ( has_nav_menu('menu_1') ) :
		        wp_nav_menu( array('theme_location' => 'menu_1', 'menu_class' => 'menu__nav') );
		    endif;
		    ?>
		</div>
	</div>

	<div class="rodape__copyright">
		<p>2017 . Laticinios Valemilk . Todos os diretos reservados</p>
	</div>

	<div class="rodape__infos">
		<div class="container">
			<address class="rodape__end">
				<p>Avenida juscelino kubitscheck s/n Ombreira - Pentecoste / Ceará - Cep: 62640-00</p>
			</address>

			<div class="rodape__fone">
				<i class="icon icon-fone"></i>
				<strong>85.3290-2332</strong>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer();?>
</body>
</html>