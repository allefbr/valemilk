<?php get_template_part('templates/html','head'); ?>

<header class="topo">
    <div class="container">
        <?php
        if ( has_nav_menu('menu_1') ) :
            wp_nav_menu( array('theme_location' => 'menu_1', 'menu_class' => 'menu__nav') );
        endif;
        ?>
    </div>
</header>

<main class="main">