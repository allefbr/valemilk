<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'valemilk');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':SL#_{p,(1LUsKu0cMwFNagqnLN~QI0;P~J+:qz)/gc$<D*b%}b/@fO!2aE)YSy>');
define('SECURE_AUTH_KEY',  '<A)OUBX-&F/`R-R@]e6$>y%O@Xt>|DyM:=eX%%JCQ 2jR/|Hsv*CuS6o1B!QF(Xg');
define('LOGGED_IN_KEY',    '.XG{G`4ba=R)MIvpyBucSVA2{`h$}FjZs7}k+u)jZsuioPZRj2L)V.G}5df~i.!#');
define('NONCE_KEY',        'RJ:qb_J!i84i#c#+]mJ+=?P5!G^C,+oL>:DACpjuxZ=<+Q`EmR~D>[;O4j>A=9v/');
define('AUTH_SALT',        'E+HczG~>/mpsBBRE&O.hS.]45*HPni},+p54[IX{|VS9_`@Y)DR~MyIH,*ExuK[P');
define('SECURE_AUTH_SALT', 'SM( /#8UNWTa;hch!!Sp/C*9UCWV/8#81QRL!@L3)&BCPLkjy}r.9[yVJc PH3*U');
define('LOGGED_IN_SALT',   '4xQuW!)Z94yrM!?Y><(6]YoNl^N$)Ww25,?BQR}%<b=+]y` -mQt9WD![,E *A}x');
define('NONCE_SALT',       '=9{8c/dm= NK!Cg}1Nq*>miHFTG 2M;A_~`{IgskcuG>qA|B~[n&SuIa2I=A5D.p');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cvtt_wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
